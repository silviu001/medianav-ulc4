#!/bin/bash

base_dir=`dirname "$(readlink -f "$0")"`
sdcard_mount=`tail -n 1 /etc/mtab | cut -d" " -f2`

source $base_dir/lib.sh

sleep 5
show_popup_msg "MediaNav: Get Device Info" "Getting medianav full data for debugging." "Please be patient" &

mount_rw $sdcard_mount

{
    lsblk --help
    hmsg "fdisk -l"
    fdisk -l
    hmsg "lsblk -b"
    lsblk -b
    hmsg "/etc/mtab"
    cat /etc/mtab
    hmsg "df"
    df
    hmsg "mount"
    mount
    hmsg "env"
    env
    hmsg "ls -l /"
    ls -l /
    hmsg "ps -A"
    ps -A
    hmsg "whoami"
    whoami
    hmsg "id"
    id
    hmsg "ls -l /proc"
    ls -l /proc
    hmsg "network info"
    ip addr show
    hmsg "uname"
    uname -a
    hmsg "CPU information"
    cat /proc/cpuinfo
    hmsg "kernel modules"
    lsmod
    hmsg "List USB entries"
    lsusb -t -v
    hmsg "List USB entries verbose"
    lsusb -v
    hmsg "Capture dmesg"
    dmesg
    hmsg "netstat ports and sockets"
    netstat -na

} 2>&1 | tee -a $base_dir/exec.`date +%d_%m_%Y__%H_%M_%S`.get_data.log

/bin/sync
mount_ro $sdcard_mount
hide_popup_msg &

#reboot -d -f
