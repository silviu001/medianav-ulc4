#!/bin/bash

base_dir=`dirname "$(readlink -f "$0")"`
sdcard_mount=`cat /etc/mtab | grep '/media/usb' | cut -d" " -f2

mount -o remount,rw $sdcard_mount >/dev/kmsg

_targets = 'app bin boot Data etc lib log_emmc navi navi_rw rw_data sbin usr'

if [ ! -d $base_dir/backup ];then
    mkdir $base_dir/backup >/dev/kmsg
fi

{
    for t in ${_targets};do
	printf "\n Backing up: %s\n" "${t}"
	tar -cf $base_dir/backup/${t} /${t}
    done
    cp -vf /proc/config.gz $base_dir/backup/
} 2>&1 | tee -a $base_dir/exec.`date +%d_%m_%Y__%H_%M_%S`.backup.log

/bin/sync

mount -o remount,ro $sdcard_mount > /dev/kmsg

#reboot -d -f
