#!/bin/bash

base_dir=`dirname "$(readlink -f "$0")"`
sdcard_mount=`tail -n 1 /etc/mtab | cut -d" " -f2`

nand_dev=/dev/mmcblk0
slice_size=2000
block_size=1M

source $base_dir/lib.sh

sleep 5
show_popup_msg "MediaNav: Gathering data" "Dumping NAND flash. Operation in progress." "A new message will popup when finished." &
sleep 3
hide_popup_msg &

mount_rw $sdcard_mount

{
    
    nand_size=$(($(lsblk -bd -o SIZE ${nand_dev} | tail -1)/1024/1024))
    nand_slices=$((nand_size/slice_size))
    nand_reminder=$((nand_size % slice_size))

    hmsg "mount"
    mount
    hmsg "df"
    df
    if [ ! -d $base_dir/backup ];then
        mkdir -p $base_dir/backup
    fi
    _ns=0
    if [ $nand_reminder -eq 0 ];then
        _ns=$((nand_slices-1))
    else
        _ns=${nand_slices}
    fi
    
    echo "NAND computed size: ${nand_size}"
    echo "NAND needed slices: ${_ns}"
    echo "Slice size: ${slice_size}"
    
    for s in `seq 0 ${_ns}`;do
        mount_rw $sdcard_mount
        # wait for a lazy FS sync
        sleep 3
        _skip=$((s*slice_size))
        time dd if=${nand_dev} of=${base_dir}/backup/mmcblk0.img-${s} count=$slice_size skip=${_skip} bs=${block_size} conv=sync,noerror 
    done
    dd if=${nand_dev}boot0 of=${base_dir}/backup/mmcblk0boot0.img bs=${block_size} conv=sync,noerror 
    dd if=${nand_dev}boot1 of=${base_dir}/backup/mmcblk0boot1.img bs=${block_size} conv=sync,noerror 

    # This method is straight forward but it won't work in all the cases because 
    # dumping each slice it takes too long and a MediaNav service (not sure yet which one) 
    # will remount in `ro` SD_CARD partition causing I/O error 
    #
    #time dd if=/dev/mmcblk0 bs=1M conv=sync,noerror | split -b 2000000000 -d - $base_dir/backup/mmcblk0.bin

} 2>&1 | tee -a $base_dir/exec.`date +%d_%m_%Y__%H_%M_%S`.dump_nand.log

/bin/sync
mount_ro $sdcard_mount
show_popup_msg "MediaNav: Gathering data" "NAND flash dump complete!" "You can now remode the USB dongle." &
sleep 3
hide_popup_msg &

#reboot -d -f
