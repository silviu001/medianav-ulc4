#!/bin/bash

# the current patch works on firmware version 1.0.15.x only !!!

# patch - will patch original nngnavi and add maps
# addux - will enable two plugins
_cmd='addux'

base_dir=`dirname "$(readlink -f "$0")"`
tdir=`tail -n 1 /etc/mtab`
cdir=${tdir:10:20}

mount -o remount,rw $cdir >/dev/kmsg
mount -o remount,rw /navi >/dev/kmsg

{
ls -l /navi
echo "----------------------------"
ps -A
echo "----------------------------"
killall nngnavi
echo "----------------------------"

if [ "${_cmd}" == "patch" ];then
    # cleanup original data
    rm -fv /navi/content/building/*
    rm -fv /navi/content/poi/*
    rm -fv /navi/content/map/*
    rm -fv /navi/content/speedcam/*

    # copy new files
    cp -Rva $base_dir/navi_scripts/nng/nngnavi /navi/nngnavi
    cp -Rva $base_dir/navi_scripts/nng/license/Mega_Free_1_Year_Update_FEU_NQ_2015_Q4.lyc /navi/license/Mega_Free_1_Year_Update_FEU_NQ_2015_Q4.lyc
    cp -Rva $base_dir/navi_scripts/nng/license/Mega_Free_1_Year_Update_FEU_NQ_2018_Q3.lyc /navi/license/Mega_Free_1_Year_Update_FEU_NQ_2018_Q3.lyc
    cp -Rva $base_dir/navi_scripts/nng/content /navi/

    chown -Rfv 1004:1004 /navi/nngnavi
    chown -Rfv 1004:1004 /navi/license/Mega_Free_1_Year_Update_FEU_NQ_2015_Q4.lyc
    chown -Rfv 1004:1004 /navi/license/Mega_Free_1_Year_Update_FEU_NQ_2018_Q3.lyc
    chown -Rfv 1004:1004 /navi/content
fi

if [ "${_cmd}" == "addux" ];then
    cp -Rva $base_dir/navi_scripts/nng/ux/* /navi/ux/
    chown -Rfv 1004:1004 /navi/ux
    echo "-----------------------"
    ls -l /navi/ux
fi


} 2>&1 | tee -a $base_dir/exec.`date +%d_%m_%Y__%H_%M_%S`.navi.log

/bin/sync

mount -o remount,ro $cdir > /dev/kmsg
mount -o remount,ro /navi > /dev/kmsg

