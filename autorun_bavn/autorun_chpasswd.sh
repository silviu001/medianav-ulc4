#!/bin/bash

base_dir=`dirname "$(readlink -f "$0")"`
sdcard_mount=`tail -n 1 /etc/mtab | cut -d" " -f2`

new_passwd=123456

source $base_dir/lib.sh

sleep 5
show_popup_msg "MediaNav Tools" "Changing root password to ${new_passwd}" "Please be patient" &

mount_rw $sdcard_mount
mount_rw /

{

    echo -n "root:${new_passwd}" | chpasswd

} 2>&1 | tee -a $base_dir/exec.`date +%d_%m_%Y__%H_%M_%S`.chpasswd.log

/bin/sync

mount_ro /
mount_ro $sdcard_mount

hide_popup_msg &    

#reboot -d -f
