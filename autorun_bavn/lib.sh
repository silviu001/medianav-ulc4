function hmsg {
    printf "\n[%s] ---> %s\n\n" "`date +%d-%m-%Y__%H:%M:%S`" "$1"
}

function mount_rw {
    /bin/mount -o remount,rw $1 >/dev/kmsg
}

function mount_ro {
    /bin/mount -o remount,ro $1 >/dev/kmsg
}

function show_popup_msg {
    local _title=${1// /\\u0020}
    local _msg1=${2// /\\u0020}
    local _msg2=${3// /\\u0020}
    dbus-send --system --print-reply --type=method_call \
              --dest=com.lge.PopupManager /com/lge/PopupManager \
              com.lge.PopupManager.Service.CreatePopup \
              string:'{"type":"NOTIFICATION","data":{"title":"'$_title'","text1":"'$_msg1'","text2":"'$_msg2'","textCount":"2","timer":"0"}}'
    dbus-send --system --print-reply --type=method_call \
              --dest=com.lge.PopupManager /com/lge/PopupManager \
              com.lge.PopupManager.Service.ShowPopup int32:7000000
}

function hide_popup_msg {
    dbus-send --system --print-reply --type=method_call \
              --dest=com.lge.PopupManager /com/lge/PopupManager \
              com.lge.PopupManager.Service.DestroyPopup int32:7000000
}